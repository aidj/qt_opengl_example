#version 450 core

layout (triangles) in;
layout (points, max_vertices = 3) out;

in vec3 tes_color[];
out vec3 geom_color;

void main(void){
int i;
for
 (i = 0; i < gl_in.length(); i++)
    {
        geom_color = tes_color[i];
        gl_Position = gl_in[i].gl_Position;
        EmitVertex();
    }
}
