#version 450

out vec4 frag_colour;

in vec3 geom_color;

void main () {
  frag_colour = vec4 (geom_color, 1.0);
}
