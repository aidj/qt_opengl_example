#version 450

in vec3 vertex_position;
in vec3 vertex_colour;
layout (location = 2) in vec4 offset;

out vec3 vs_color;

void main () {
  vs_color = vertex_colour;
  gl_Position = vec4 (vertex_position, 1.0) + offset;
}
