#ifndef BASEOPENGLRENDERER_H
#define BASEOPENGLRENDERER_H

#include <memory>

#include <QOpenGLContext>
#include <QOpenGLFunctions_4_5_Core>

#include "../intf/irenderer.h"
#include "../../view/impl/openglview.h"

class BaseOpenGLRenderer : public IRenderer
{
public:
    BaseOpenGLRenderer(OpenGLView& view, QString&& pathToShaders);
    ~BaseOpenGLRenderer() override {}

    // IRenderer interface
public:
    bool init() override;

protected:
    virtual bool internalInit() = 0;

    std::shared_ptr<QOpenGLContext> m_context;
    QString m_pathToShaders;
    bool m_isInited;

    std::shared_ptr<QOpenGLFunctions_4_5_Core> m_funcs;
    OpenGLView& m_view;
};

#endif // BASEOPENGLRENDERER_H
