#ifndef SIMPLEOPENGLRENDERER_H
#define SIMPLEOPENGLRENDERER_H

#include <memory>

#include <QString>

#include "../../view/impl/openglview.h"
#include "baseopenglrenderer.h"

class SimpleOpenGLRenderer : public BaseOpenGLRenderer
{
public:
    SimpleOpenGLRenderer(OpenGLView& view, QString&& pathToShaders);
    ~SimpleOpenGLRenderer() override {}

    // IRenderer interface
public:
    void render() override;
    void resize(int width, int height) override;
    void deinit() override;

    // BaseOpenGLRenderer interface
protected:
    bool internalInit() override;

private:
    bool initVBOs();
    bool initVertexVBO();
    bool initColourVBO();
    bool initVAO();
    bool initShaderProgram();

    static const int vertex_position_location = 0;
    static const int vertex_colour_location = 1;

    QOpenGLBuffer m_vertexVBO;
    QOpenGLBuffer m_colourVBO;
    QOpenGLVertexArrayObject m_vao;
    std::unique_ptr<QOpenGLShaderProgram> m_shaderProgram;
};

#endif // SIMPLEOPENGLRENDERER_H
