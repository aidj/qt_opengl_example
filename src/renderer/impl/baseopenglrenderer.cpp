#include "baseopenglrenderer.h"

BaseOpenGLRenderer::BaseOpenGLRenderer(
        OpenGLView &view,
        QString&& pathToShaders)
    : IRenderer(),
      m_context(nullptr),
      m_pathToShaders(pathToShaders),
      m_isInited(false),
      m_funcs(nullptr),
      m_view(view)
{}

bool BaseOpenGLRenderer::init()
{
    if(!m_isInited) {
        m_context = m_view.getContext();
        m_funcs = m_view.getFuncs();
        m_isInited = internalInit();
        return m_isInited;
    }
    return true;
}
