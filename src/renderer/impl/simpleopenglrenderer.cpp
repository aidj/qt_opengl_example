#include "simpleopenglrenderer.h"

#include <qlogging.h>
#include <QDateTime>

SimpleOpenGLRenderer::SimpleOpenGLRenderer(
        OpenGLView &view,
        QString&& pathToShaders)
    : BaseOpenGLRenderer(view, std::move(pathToShaders)),
      m_vertexVBO(QOpenGLBuffer::VertexBuffer),
      m_colourVBO(QOpenGLBuffer::VertexBuffer),
      m_vao(),
      m_shaderProgram(nullptr)
{}

void SimpleOpenGLRenderer::render()
{
    if(!m_isInited) {
        return;
    }

    // Clear the buffer with the current clearing color
    m_funcs->glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    static const GLfloat red[] = { 0.1f, 0.1f, 0.1f, 1.0f };
    m_funcs->glClearBufferfv(GL_COLOR, 0, red);

    if(!m_shaderProgram->bind()) {
        qCritical()<<"Could not bind shader program. Log: "<< m_shaderProgram->log();
        return;
    }

    QOpenGLVertexArrayObject::Binder binder( &m_vao );

    GLfloat attrib[] = { (float)sin(QDateTime::currentMSecsSinceEpoch()%60000/100) * 0.5f,
                         (float)cos(QDateTime::currentMSecsSinceEpoch()%60000/100) * 0.6f,
                         0.0f, 0.0f };
    m_funcs->glVertexAttrib4fv(2, attrib);

    m_funcs->glDrawArrays(GL_PATCHES/*GL_TRIANGLES*/, 0, 3);
    m_shaderProgram->release();
}

void SimpleOpenGLRenderer::resize(int, int)
{
    if(!m_isInited) {
        return;
    }
}

void SimpleOpenGLRenderer::deinit()
{
    if(m_isInited) {
        m_vao.destroy();
        m_colourVBO.destroy();
        m_vertexVBO.destroy();
        m_shaderProgram = nullptr;
        m_isInited = false;
    }
}

bool SimpleOpenGLRenderer::internalInit()
{
    qDebug() << "OpenGL information: VENDOR:       " << (const char*)m_funcs->glGetString(GL_VENDOR);
    qDebug() << "                    RENDERDER:    " << (const char*)m_funcs->glGetString(GL_RENDERER);
    qDebug() << "                    VERSION:      " << (const char*)m_funcs->glGetString(GL_VERSION);
    qDebug() << "                    GLSL VERSION: " << (const char*)m_funcs->glGetString(GL_SHADING_LANGUAGE_VERSION);

    m_funcs->glEnable(GL_DEPTH_TEST);
    m_funcs->glDepthFunc (GL_LESS);

    if(!initShaderProgram() || !initVBOs() || !initVAO()) {
        return false;
    }

    m_funcs->glClearColor(0.2f, 0.0f, 0.5f, 1.0f);
    m_funcs->glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    //m_funcs->glCullFace (GL_BACK); // cull back face

    return true;
}

bool SimpleOpenGLRenderer::initVertexVBO()
{
    float points[] = {
       -0.5f, -0.5f,  0.0f,
       0.5f, -0.5f,  0.0f,
       0.0f,  0.5f,  0.0f
    };

    if(!m_vertexVBO.create()) {
        qCritical("Could not create vertex VBO.");
        return false;
    }
    m_vertexVBO.setUsagePattern(QOpenGLBuffer::StaticDraw);
    if(!m_vertexVBO.bind())
    {
        qCritical("Could not bind vertex buffer to the context");
        m_vertexVBO.destroy();
        return false;
    }
    m_vertexVBO.allocate(points, 9 * sizeof (float));
    m_vertexVBO.release();

    return true;
}

bool SimpleOpenGLRenderer::initColourVBO()
{
    float colours[] = {
       0.0f, 0.0f,  1.0f,
       0.0f, 1.0f,  0.0f,
       1.0f, 0.0f,  0.0f
    };

    if(!m_colourVBO.create()) {
        qCritical("Could not create colour VBO.");
        return false;
    }
    m_colourVBO.setUsagePattern(QOpenGLBuffer::StaticDraw);
    if(!m_colourVBO.bind())
    {
        qCritical("Could not bind colour buffer to the context");
        m_colourVBO.destroy();
        return false;
    }
    m_colourVBO.allocate(colours, 9 * sizeof (float));
    m_colourVBO.release();

    return true;
}

bool SimpleOpenGLRenderer::initVBOs()
{
    return initVertexVBO() && initColourVBO();
}

bool SimpleOpenGLRenderer::initVAO()
{
    if(!m_vao.create()) {
        qCritical("Could not create VAO.");
        return false;
    }
    QOpenGLVertexArrayObject::Binder binder(&m_vao);
    if(!m_shaderProgram->bind())
    {
        qCritical() << "Could not bind shader program to context";
        return false;
    }

    if(!m_vertexVBO.bind())
    {
        qCritical("Could not bind vertex buffer to the context");
        return false;
    }
    m_shaderProgram->setAttributeBuffer(vertex_position_location, GL_FLOAT, 0, 3);
    m_shaderProgram->enableAttributeArray(vertex_position_location);
    m_vertexVBO.release();

    if(!m_colourVBO.bind())
    {
        qCritical("Could not bind colour buffer to the context");
        return false;
    }
    m_shaderProgram->setAttributeBuffer(vertex_colour_location, GL_FLOAT, 0, 3);
    m_shaderProgram->enableAttributeArray(vertex_colour_location);
    m_colourVBO.release();

    m_shaderProgram->release();

    return true;
}

bool SimpleOpenGLRenderer::initShaderProgram()
{
    m_shaderProgram = std::make_unique<QOpenGLShaderProgram>(nullptr);

    if(!m_shaderProgram->addShaderFromSourceFile(
                QOpenGLShader::Vertex,
                m_pathToShaders + "simple.vert")) {
        qCritical()<<"Could not compile Vertex shader. Log: " << m_shaderProgram->log();
        return false;
    }

    if(!m_shaderProgram->addShaderFromSourceFile(
                QOpenGLShader::TessellationControl,
                m_pathToShaders + "simple.tescon")) {
        qCritical()<<"Could not compile TessellationControl shader. Log: " << m_shaderProgram->log();
        return false;
    }

    if(!m_shaderProgram->addShaderFromSourceFile(
                QOpenGLShader::TessellationEvaluation,
                m_pathToShaders + "simple.tesev")) {
        qCritical()<<"Could not compile TessellationEvaluation shader. Log: " << m_shaderProgram->log();
        return false;
    }

    if(!m_shaderProgram->addShaderFromSourceFile(
                QOpenGLShader::Fragment,
                m_pathToShaders + "simple.frag")) {
        qCritical()<<"Could not compile Fragment shader. Log: " << m_shaderProgram->log();
        return false;
    }

    if(!m_shaderProgram->addShaderFromSourceFile(
                QOpenGLShader::Geometry,
                m_pathToShaders + "simple.geom")) {
        qCritical()<<"Could not compile Geometry shader. Log: " << m_shaderProgram->log();
        return false;
    }

    if(!m_shaderProgram->link()) {
        qCritical()<<"Could not link shader program. Log: " << m_shaderProgram->log();
        return false;
    }

    return true;
}
