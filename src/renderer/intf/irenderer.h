#ifndef IRENDERER_H
#define IRENDERER_H

class IRenderer
{
public:
    virtual ~IRenderer() {}
    virtual bool init() = 0;
    virtual void render() = 0;
    virtual void resize(int width, int height) = 0;
    virtual void deinit() = 0;
};

#endif // IRENDERER_H
