#include "openglview.h"

#include <math.h>

#include <QSurfaceFormat>
#include <qlogging.h>
#include <QDateTime>

OpenGLView::OpenGLView()
    : QWindow(),
      m_context(nullptr),
      m_funcs(nullptr),      
      m_logger(nullptr),
      m_renderers(),
      m_size(0, 0)
{
}

OpenGLView::~OpenGLView()
{
    deinit();
}

std::shared_ptr<QOpenGLContext> OpenGLView::getContext()
{
    return m_context;
}

std::shared_ptr<QOpenGLFunctions_4_5_Core> OpenGLView::getFuncs()
{
    return m_funcs;
}

bool OpenGLView::initFuncs()
{
    auto funcs = m_context->versionFunctions<QOpenGLFunctions_4_5_Core>();
    if (funcs == nullptr) {
        qCritical("Could not get OpenGL function pointer");
        return false;
    }
    m_funcs = std::unique_ptr<QOpenGLFunctions_4_5_Core>(funcs);
    return m_funcs->initializeOpenGLFunctions();
}

bool OpenGLView::setCurrentContext()
{
    if(!isExposed()) {
        qWarning() << "Window isn't exposed.";
        return false;
    }
    if(m_context == nullptr) {
        qWarning() << "OpenGL context isn't inited.";
        return false;
    }
    if(!m_context->makeCurrent(this)) {
        qWarning() << "Could not make current OpenGL context.";
        return false;
    }

    return true;
}

bool OpenGLView::init()
{
    if(m_context != nullptr) {
        return false;
    }

    // Tell Qt we will use OpenGL for this window
    setSurfaceType( OpenGLSurface );

    // Specify the format and create platform-specific surface
    QSurfaceFormat format;
    format.setDepthBufferSize( 24 );
    format.setMajorVersion( 4 );
    format.setMinorVersion( 5 );
    format.setSamples( 4 );
    format.setProfile( QSurfaceFormat::CoreProfile );
    format.setOption(QSurfaceFormat::DebugContext);
    setFormat( format );
    create();

    m_context = std::make_shared<QOpenGLContext>(nullptr);
    m_context->setFormat( format );
    if(!m_context->create()) {
        qCritical()<<"Could not create OpenGL context.";
        m_context = nullptr;
        return false;
    }
    if(!m_context->makeCurrent(this)) {
        qCritical()<<"Could not make current OpenGL context.";
        m_context = nullptr;
        return false;
    }

    qDebug() << "Used OpenGL: " << m_context->format().majorVersion()
             << "." << m_context->format().minorVersion();

    if(!initFuncs()) {
        return false;
    }

    for(auto& renderer : m_renderers) {
        if(!renderer.second->init()) {
            qCritical()<<"Could not init rederer with id: " << renderer.first;
            m_context = nullptr;
            return false;
        }
    }

    m_context->doneCurrent();

    return true;
}

void OpenGLView::render()
{
    if(setCurrentContext()) {
        if(m_size.first == 0 || m_size.second == 0) {
            resize(512, 512);
            return;
        }

        for(auto& renderer : m_renderers) {
            renderer.second->render();
        }

        m_context->swapBuffers(this);
        m_context->doneCurrent();
    }
}

bool OpenGLView::resize(int width, int height)
{
    if(width < 1 || height < 1) {
        qCritical() <<"width: " << width
                    << " and height: " << height
                    << " can't be less that 1";
        return false;
    }
    if(!setCurrentContext()) {
        return false;
    }
    QWindow::resize(width, height);
    m_funcs->glViewport(0, 0, width, height);

    for(auto& renderer : m_renderers) {
        renderer.second->resize(width, height);
    }

    m_context->doneCurrent();
    m_size.first = width;
    m_size.second = height;
    return true;
}

void OpenGLView::deinit()
{
    for(auto& renderer : m_renderers) {
        renderer.second->deinit();
    }

    m_context = nullptr;
    m_funcs = nullptr;
    enableLogging(false);
}

bool OpenGLView::enableLogging(bool enable)
{
    if(enable) {
        if(m_context == nullptr) {
            qWarning() << "OpenGL context isn't inited.";
            return false;
        }
        if(!m_context->makeCurrent(this) || !m_context->hasExtension(QByteArrayLiteral("GL_KHR_debug"))) {
            return false;
        }

        m_logger = std::make_unique<QOpenGLDebugLogger>(nullptr);
        m_logger->initialize();
        connect(m_logger.get(), &QOpenGLDebugLogger::messageLogged, this, &OpenGLView::log);
        m_logger->startLogging();
        m_context->doneCurrent();
        return true;
    } else {
        m_logger->stopLogging();
        disconnect(m_logger.get(), &QOpenGLDebugLogger::messageLogged, this, &OpenGLView::log);
        m_logger = nullptr;
        return true;
    }
}

bool OpenGLView::addRenderer(
        unsigned int id, std::unique_ptr<IRenderer>&& renderer)
{
    auto hasId = m_renderers.find(id) != m_renderers.end();
    if(hasId) {
        return false;
    }

    if(m_context != nullptr) {
        renderer->init();
    }
    if(m_size.first > 0 && m_size.second > 0) {
        renderer->resize(m_size.first, m_size.second);
    }
    m_renderers[id] = std::move(renderer);
    return true;
}

void OpenGLView::removeRenderer(unsigned int id)
{
    if(m_context != nullptr) {
        m_renderers[id]->deinit();
    }
    m_renderers.erase(id);
}

void OpenGLView::log(const QOpenGLDebugMessage &debugMessage)
{
    qDebug() << debugMessage;
}
