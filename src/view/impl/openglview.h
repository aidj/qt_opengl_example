#ifndef OPENGLVIEW_H
#define OPENGLVIEW_H

#include <memory>

#include <QWindow>
#include <QOpenGLContext>
#include <QOpenGLFunctions_4_5_Core>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLShaderProgram>
#include <QOpenGLDebugLogger>
#include <QOpenGLDebugMessage>
#include <QString>

#include "../../renderer/intf/irenderer.h"

class OpenGLView : public QWindow
{
    Q_OBJECT
public:
    OpenGLView();
    virtual ~OpenGLView();

    std::shared_ptr<QOpenGLContext> getContext();
    std::shared_ptr<QOpenGLFunctions_4_5_Core> getFuncs();

    bool init();
    void render();
    bool resize(int width, int height);
    void deinit();
    bool enableLogging(bool enable);
    bool addRenderer(unsigned int id, std::unique_ptr<IRenderer>&& renderer);
    void removeRenderer(unsigned int id);

private slots:
    void log(const QOpenGLDebugMessage &debugMessage);

private:
    bool initFuncs();
    bool setCurrentContext();

    std::shared_ptr<QOpenGLContext> m_context;
    std::shared_ptr<QOpenGLFunctions_4_5_Core> m_funcs;

    std::unique_ptr<QOpenGLDebugLogger> m_logger;
    std::map<unsigned int, std::unique_ptr<IRenderer>> m_renderers;
    std::pair<int, int> m_size;
};

#endif // OPENGLVIEW_H
