#-------------------------------------------------
#
# Project created by QtCreator 2016-09-05T19:20:56
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG   += c++14
QMAKE_CXXFLAGS += -std=c++14

TARGET = TestOpenGL
TEMPLATE = app


SOURCES += main.cpp \
    src/renderer/impl/baseopenglrenderer.cpp \
    src/view/impl/openglview.cpp \
    src/renderer/impl/simpleopenglrenderer.cpp

resources.path = $$OUT_PWD/resources
resources.files += resources/*

INSTALLS += resources

DISTFILES += \
    resources/graphics/shaders/simple_renderer/simple.geom \
    resources/graphics/shaders/simple_renderer/simple.tescon \
    resources/graphics/shaders/simple_renderer/simple.tesev \
    resources/graphics/shaders/simple_renderer/simple.frag \
    resources/graphics/shaders/simple_renderer/simple.vert

HEADERS += \
    src/renderer/impl/baseopenglrenderer.h \
    src/renderer/intf/irenderer.h \
    src/view/impl/openglview.h \
    src/renderer/impl/simpleopenglrenderer.h
