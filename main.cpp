#include <QApplication>
#include <QWindow>

#include <QTimer>

#include "src/renderer/impl/simpleopenglrenderer.h"
#include "src/view/impl/openglview.h"

bool simpleDemo(OpenGLView& view) {
    return view.addRenderer(
                0,
                std::make_unique<SimpleOpenGLRenderer>(
                    view, QString("resources/graphics/shaders/simple_renderer/")
                    )
                );
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    OpenGLView view;

    if(!simpleDemo(view)) {
        return -1;
    }

    view.init();
    view.enableLogging(true);    
    view.show();    

    QTimer timer;
    QObject::connect(&timer, &QTimer::timeout, &view, &OpenGLView::render);
    timer.start(16);

    return a.exec();
}
